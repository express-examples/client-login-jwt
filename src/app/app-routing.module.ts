import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './pages/main/main.component';
import { LayoutComponent } from './pages/layout/layout.component';
import { LoginComponent } from './pages/login/login.component';
import { LogoutComponent } from './pages/logout/logout.component';
import { RegisterComponent } from './pages/register/register.component';
import { PrivateComponent } from './pages/private/private.component';
import { PublicComponent } from './pages/public/public.component';
import { LayoutAuthComponent } from './pages/layout-auth/layout-auth.component';
import { IsLoginGuard } from './guards/is-login.guard';


const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      {path: '', component: MainComponent},
      {path: 'privado', component: PrivateComponent, canActivate: [IsLoginGuard]},
      {path: 'publico', component: PublicComponent},
    ],
  },
  {
    path: 'auth', component: LayoutAuthComponent,
    children: [
      {path: 'login', component: LoginComponent},
      {path: 'logout', component: LogoutComponent},
      {path: 'register', component: RegisterComponent},
    ],
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.sass']
})
export class LayoutComponent implements OnInit {
  user: any;
  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() {
    this.userService.getUser().subscribe(
      (user) => this.user = user,
    );
  }

}

import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  constructor(
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  login(email: string, password: string): void {
    this.authService.login({email, password}).subscribe(
      (response) => {
        this.userService.byToken(response.token);
        this.router.navigate(['/']);
      },
      (err) => console.error(err),
    );
  }

}
